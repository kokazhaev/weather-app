import styled from 'styled-components';

export const Wrap = styled.div`
  width: 1150px;
  margin: 0 auto;
  text-align: center;
`;

export const Error = styled.div`
  font-size: 18px;
  font-family: arial,sans-serif;
  margin-bottom: 10px;
  color: #e34b4b;
`