import styled, { keyframes } from 'styled-components';

const lds_double_ring = keyframes` 
  0% {
    -webkit-transform: rotate(0);
    transform: rotate(0);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
`
export const Wrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 50px;
  & > svg {
    width: 40px;
    height: 40px;
    margin-right: 10px;
    animation: ${lds_double_ring} 1s linear infinite;
  }
`
export const Text = styled.div`
  font-family: 'LatoWeb', sans-serif;
  font-size: 15px;
  color: #3F3356;
`