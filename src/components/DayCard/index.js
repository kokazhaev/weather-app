import React from 'react';
import dayOfWeek from '../dayOfWeek';
// styles
import { Wrap, Text, TextWrapper, Icon } from './style';

const DayCard = ({ data }) => {
  const date = new Date(data.datetime).getDay()

  return (
    <Wrap>
      <Text>{dayOfWeek(date)}</Text>
      <Icon>
        <img src={`https://www.weatherbit.io/static/img/icons/${data.weather.icon}.png`} alt='logo' />
      </Icon>
      <TextWrapper>
        <Text>{ data.high_temp } ° </Text>
        <Text color='#878787'>{ data.low_temp } °</Text>
      </TextWrapper>
    </Wrap>
  )
}

export default DayCard;