import styled from 'styled-components';

export const Wrap = styled.div`
  text-align: center;
  margin-right: 10px;
  &:first-child {
    border: 1px solid #eeeeef;
    padding: 10px;
    border-radius: 5px;
    background: #fcfcfc;
  }
  padding: 10px
`;

export const Text = styled.div`
  font-size: 12px;
  font-family: arial,sans-serif;
  color: ${props => props.color};
  margin-right: 5px;
`;  

export const TextWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

export const Icon = styled.div`
  width: 80px;
  & > img {
    width: 100%;
  }
`;