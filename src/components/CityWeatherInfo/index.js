import React from 'react';
import dayOfWeek from '../dayOfWeek';

// components
import OtherDays from '../OtherDays';

// styles
import { 
  Wrap, 
  Title, 
  WeatherInfo,
  InfoBlock,
  Icon,
  Text
} from './style';

const CityWeatherInfo = ({ data }) => {
  const { city_name, country_code } = data;
  const today = data.data[0]
  const { icon, description } = today.weather; 
  const date = new Date(today.datetime).getDay()
  const year = new Date(today.datetime).getFullYear()

  return (
    <Wrap>
      <Title>{ city_name }, { country_code }</Title>
      <Text fs='13px'>{ dayOfWeek(date) }, { year }</Text>
      <Text fs='13px'>{ description }</Text>
      <WeatherInfo>
        <InfoBlock display='flex'>
          <Icon>
            <img src={`https://www.weatherbit.io/static/img/icons/${icon}.png`} alt='logo' />
          </Icon>
          <Text fs='35px' fw='600'>{ today.high_temp } °C</Text>
        </InfoBlock>

        <InfoBlock>
          <Text fs='14px'>Wind speed: { today.wind_spd.toFixed(1) } m/s</Text>
          <Text fs='14px'>Probability of precipitation: { today.pop } %</Text>
          <Text fs='14px'>Average pressure: { today.pres.toFixed(1) } mb</Text>
        </InfoBlock>
      </WeatherInfo>
      <OtherDays list={data.data} />
    </Wrap>
  )
}

export default CityWeatherInfo;