import styled from 'styled-components';

export const Wrap = styled.div`
  width: 600px;
  margin: 0 auto;
  border: 1px solid #dfe1e5;;
  border-radius: 5px;
  box-sizing: border-box;
  padding: 20px;
`;

export const Title = styled.div`
  color: #878787;
  font-size: 22px;
  font-family: arial,sans-serif;
  margin-bottom: 5px;
`;

export const WeatherInfo = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 40px;
`;

export const InfoBlock = styled.div`
  display: ${props => props.display};
  align-items: center;
  justify-content: flex-start;
`;

export const Icon = styled.div`
  margin-right: 10px;
`;

export const Text = styled.div`
  font-size: ${props => props.fs};
  font-family: arial,sans-serif;
  color: #222;
  font-weight: ${props => props.fw};
  margin-bottom: ${props => props.mb || '5px'};
  text-align: left !important; 
`;