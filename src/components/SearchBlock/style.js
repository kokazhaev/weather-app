import styled from 'styled-components';

export const Wrap = styled.div`
  width: 400px;
  height: 40px;
  display: flex;
  justify-content: space-between;
  margin: 0 auto 20px;
  & > input {
    width: 70%;
    height: 100%;
    outline: none;
    border: 1px solid #eeeeef;
    border-radius: 5px;
    box-sizing: border-box;
    padding: 0 10px;
    font-size: 14px;
  }
`