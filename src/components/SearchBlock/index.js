import React from 'react';

// components
import Button from '../Button';

// styles
import {
  Wrap
} from './style';

const SearchBlock = ({cityName, setCityName, getWeatherData}) => {
  return (
    <Wrap>
      <input onChange={(e) => setCityName(e.target.value) } />
      <Button onClick={() => getWeatherData(cityName)}>search</Button>
    </Wrap>
  )
}

export default SearchBlock;