import styled from 'styled-components';

export const Wrap = styled.button`
  width: ${props => props.width || '100px'};
  height: ${props => props.height || 'auto'};
  background: ${props => props.color || 'none'};
  border: ${props => props.border || '1px solid #eeeeef'};
  color: ${props => props.fontColor || 'rgb(40, 47, 54);'};
  font-size: 14px;
  font-family: 'Lato', sans-serif;
  outline: none;
  cursor: pointer;
  border-radius: ${props => (props.radius && props.radius) || '7px'};
  display: block;
  text-align: center;
  padding: ${props => props.padding};
  margin-top: ${props => props.top || '0'};
  margin-left: ${props => props.left || '0'};
  margin-right: ${props => props.right || '0'};
  margin-bottom: ${props => props.bottom || '0'};
  &:active{
    background: silver
  }
`