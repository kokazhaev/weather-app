import React from 'react';

// styles
import { Wrap } from './style';

const Button = props => {
  return <Wrap {...props}>{props.children}</Wrap>
}

export default Button;