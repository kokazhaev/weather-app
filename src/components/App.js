import React, { useState, useEffect } from 'react';
import axios from 'axios';

// components
import SearchBlock from './SearchBlock';
import CityWeatherInfo from './CityWeatherInfo';
import Loader from './Loader';

// styles
import { Wrap, Error } from './style';

const App = () => {
  const [cityName, setCityName] = useState('almaty');
  const [weatherData, setWeatherData] = useState(null);
  const [incorrectCity, setIncorrectCity] = useState(false);

  // key for weather api
  const API_KEY = '6b43959d5d8a451089080ca18edfc935';
  const apiUrl = `https://api.weatherbit.io/v2.0/forecast/daily?city=${cityName}&key=${API_KEY}`;

  useEffect(() => {
    getWeatherData()
  }, []);

  const getWeatherData = async () => {
    try {
      const city = await axios.get(apiUrl);
      // если статус 200 значит вернул правилные данные
      if(city.status === 200) {
        setIncorrectCity(false);
        setWeatherData(city.data);
      }
      // если 204, значит вернул пустую строку
      if (city.status === 204) {
        console.error('Empty data');
        setIncorrectCity(true);  
      }
    } catch (err) {
      console.log('err', err);
    }
  }

  return (
    <Wrap>
      <SearchBlock cityName={cityName} setCityName={setCityName} getWeatherData={getWeatherData} />
      { incorrectCity && <Error>Incorrect city</Error> }
      { !weatherData ? (<Loader />) : (<CityWeatherInfo data={weatherData} />) }
    </Wrap>
  )
}

export default App;