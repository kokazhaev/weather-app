import styled from 'styled-components';

export const Wrap = styled.div`
  display: flex;
  overflow: scroll;
  &::-webkit-scrollbar { 
    display: none; 
  }
`;