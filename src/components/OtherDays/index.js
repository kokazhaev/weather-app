import React from 'react';

// components
import DayCard from '../DayCard';

// styles
import {
  Wrap
} from './style';

const OtherDays = ({ list }) => {

  return (
    <Wrap>
      { list.map(item => <DayCard key={item.datetime} data={item} />) }
    </Wrap>
  )
}

export default OtherDays;